const WARN = 1;

module.exports = {
  extends: ['prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': [
      WARN,
      {
        printWidth: 120,
        singleQuote: true,
        trailingComma: 'none',
        jsxBracketSameLine: true,
        arrowParens: 'avoid'
      }
    ]
  }
};
