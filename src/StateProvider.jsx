import { createContext, createEffect, createSignal, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';

const store = createContext();

const { Provider } = store;

export const useStore = () => {
  return useContext(store);
};

const StateProvider = props => {
  const [currentIdx, setCurrentIdx] = createSignal(0);
  const [state, setState] = createStore({
    email: 'morty.smith@pm.me'
  });

  createEffect(() => {
    console.log('currentIdx', currentIdx());
  });

  const value = [
    {
      currentIdx,
      fieldValues: state
    },
    {
      incrementIndex() {
        setCurrentIdx(prevCurrentIdx => prevCurrentIdx + 1);
      },
      decrementIndex() {
        setCurrentIdx(prevCurrentIdx => prevCurrentIdx - 1);
      },
      getField(key) {
        return state[key] || '';
      },
      setField(key, value) {
        setState({ [key]: value });
      }
    }
  ];

  return <Provider value={value}>{props.children}</Provider>;
};

export default StateProvider;
