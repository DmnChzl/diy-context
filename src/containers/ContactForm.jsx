import Form from '../components/Form';
import InputField from '../components/InputField';
import { useStore } from '../StateProvider';

const ContactForm = props => {
  const [{ fieldValues }, { decrementIndex, getField, setField }] = useStore();

  return (
    <Form {...props} handlePrevious={decrementIndex}>
      <InputField
        label="Email"
        type="email"
        placeholder="rick.sanchez@pm.me"
        defaultValue={getField('email')}
        handleChange={e => setField('email', e.target.value)}
      />
    </Form>
  );
};

export default ContactForm;
