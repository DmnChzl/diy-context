import Form from '../components/Form';
import SelectField from '../components/SelectField';
import InputField from '../components/InputField';
import { useStore } from '../StateProvider';

function ProForm(props) {
  const [{ fieldValues }, { incrementIndex, decrementIndex, getField, setField }] = useStore();

  return (
    <Form
      {...props}
      handlePrevious={decrementIndex}
      handleNext={incrementIndex}
      isDisabled={!fieldValues['stack'] || !fieldValues['language']}>
      <SelectField
        label="Stack"
        options={['Front', 'Back', 'FullStack']}
        defaultValue={getField('stack')}
        isRequired
        handleChange={e => setField('stack', e.target.value)}
      />
      <SelectField
        label="Language"
        options={['JavaScript', 'Python', 'Java', 'PHP', 'Other']}
        defaultValue={getField('language')}
        isRequired
        handleChange={e => setField('language', e.target.value)}
      />
      <InputField
        label="Other"
        placeholder="Go, Rust..."
        defaultValue={getField('other')}
        handleChange={e => setField('other', e.target.value)}
      />
    </Form>
  );
}

export default ProForm;
