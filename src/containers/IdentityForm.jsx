import Form from '../components/Form';
import InputField from '../components/InputField';
import { useStore } from '../StateProvider';

const IdentityForm = props => {
  const [{ fieldValues }, { incrementIndex, getField, setField }] = useStore();

  return (
    <Form {...props} handleNext={incrementIndex} isDisabled={!fieldValues['lastName'] || !fieldValues['firstName']}>
      <InputField
        label="LastName"
        placeholder="Carniato"
        defaultValue={getField('lastName')}
        isRequired
        handleChange={e => setField('lastName', e.target.value)}
      />
      <InputField
        label="FirstName"
        placeholder="Ryan"
        defaultValue={getField('firstName')}
        isRequired
        handleChange={e => setField('firstName', e.target.value)}
      />
    </Form>
  );
};

export default IdentityForm;
