import Form from '../components/Form';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import { useStore } from '../StateProvider';

const FantasyForm = props => {
  const [{ fieldValues }, { incrementIndex, decrementIndex, getField, setField }] = useStore();

  return (
    <Form
      {...props}
      handlePrevious={decrementIndex}
      handleNext={incrementIndex}
      isDisabled={!fieldValues['job'] || !fieldValues['element']}>
      <InputField
        label="NickName"
        placeholder="Cliff"
        defaultValue={getField('nickName')}
        handleChange={e => setField('nickName', e.target.value)}
      />
      <SelectField
        label="Job"
        options={['Bowman', 'Knight', 'Paladin', 'Wizard']}
        defaultValue={getField('job')}
        isRequired
        handleChange={e => setField('job', e.target.value)}
      />
      <SelectField
        label="Element"
        options={['Fire', 'Rock', 'Earth', 'Wind', 'Water', 'Ice', 'Lightning']}
        defaultValue={getField('element')}
        isRequired
        handleChange={e => setField('element', e.target.value)}
      />
    </Form>
  );
};

export default FantasyForm;
