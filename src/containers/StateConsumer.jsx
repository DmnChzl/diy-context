import { Switch, Match } from 'solid-js';
import Elevation from '../components/Elevation';
import LoremIpsum from '../components/LoremIpsum';
import IdentityForm from './IdentityForm';
import ProForm from './ProForm';
import FantasyForm from './FantasyForm';
import ContactForm from './ContactForm';
import { useStore } from '../StateProvider';

const getClass = idx => {
  switch (idx) {
    case 0:
      return 'is-blue';
    case 1:
      return 'is-red';
    case 2:
      return 'is-yellow';
    case 3:
      return 'is-green';
    default:
      return '';
  }
};

const StateConsumer = () => {
  const [{ currentIdx }] = useStore();

  return (
    <section class={`container ${getClass(currentIdx())}`}>
      <div class="columns">
        <Elevation deskSize={8} tabSize={10} isReversed={currentIdx() % 2 !== 0}>
          <div class="column is-left">
            <Switch>
              <Match when={currentIdx() % 2 === 0}>
                <LoremIpsum />
              </Match>
              <Match when={currentIdx() === 1}>
                <ProForm title="Professional" description="Let's talk about your work..." />
              </Match>
              <Match when={currentIdx() === 3}>
                <ContactForm title="Contact" description="Finally, leave your contact details..." />
              </Match>
            </Switch>
          </div>

          <div class="column is-right">
            <Switch>
              <Match when={currentIdx() % 2 !== 0}>
                <LoremIpsum />
              </Match>
              <Match when={currentIdx() === 0}>
                <IdentityForm title="Identity" description="Who are you?" />
              </Match>
              <Match when={currentIdx() === 2}>
                <FantasyForm title="Fantasy" description="In a parallel universe, who would you be?" />
              </Match>
            </Switch>
          </div>
        </Elevation>
      </div>
    </section>
  );
};

export default StateConsumer;
