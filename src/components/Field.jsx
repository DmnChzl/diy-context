import { mergeProps } from 'solid-js';

const Field = props => {
  const merged = mergeProps({ isRequired: false }, props);

  return (
    <div class="field">
      <div class="label">
        {props.label}
        {merged.isRequired ? ' *' : ''}
      </div>
      <div class="control">{props.children}</div>
    </div>
  );
};

export default Field;
