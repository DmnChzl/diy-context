import { mergeProps } from 'solid-js';

const Form = props => {
  const merged = mergeProps({ isDisabled: false }, props);

  return (
    <div class="with-form">
      <h3 class="title is-4">{props.title}</h3>
      <p class="description">{props.description}</p>
      <form
        onSubmit={e => {
          e.preventDefault();
          props.handleNext && props.handleNext();
        }}>
        {props.children}
        <div class="columns buttons">
          {props.handlePrevious && (
            <div class="column">
              <button class="button is-primary is-fullwidth" type="button" onClick={props.handlePrevious}>
                Previous
              </button>
            </div>
          )}

          {props.handleNext && (
            <div class="column">
              <button class="button is-primary is-fullwidth" type="submit" disabled={merged.isDisabled}>
                Next
              </button>
            </div>
          )}
        </div>
      </form>
    </div>
  );
};

export default Form;
