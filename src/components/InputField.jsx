import { mergeProps } from 'solid-js';
import Field from './Field';

const InputField = props => {
  const merged = mergeProps({ type: 'text', isRequired: false }, props);

  return (
    <Field label={props.label} isRequired={merged.isRequired}>
      <input
        class="input"
        type={merged.type}
        placeholder={props.placeholder}
        value={props.defaultValue}
        onInput={props.handleChange}
      />
    </Field>
  );
};

export default InputField;
