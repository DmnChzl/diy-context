const LoremIpsum = () => (
  <>
    <h1 class="title is-2">State Management</h1>
    <h2 class="subtitle colored is-3">Home Made</h2>
    <p>
      State Management is a reactive programming approach. In the Solid ecosystem, this concept is associated to
      Context...
      <br />
      But, you can do it yourself!
    </p>
  </>
);

export default LoremIpsum;
