import { mergeProps, For } from 'solid-js';
import Field from './Field';

const SelectField = props => {
  const merged = mergeProps({ options: [], isRequired: false }, props);

  return (
    <Field label={props.label} isRequired={merged.isRequired}>
      <div class="select is-fullwidth">
        <select onChange={props.handleChange} value={props.defaultValue}>
          <option value="" disabled>
            Unknown
          </option>
          <For each={merged.options}>{option => <option value={option}>{option}</option>}</For>
        </select>
      </div>
    </Field>
  );
};

export default SelectField;
