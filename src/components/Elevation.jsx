import { mergeProps } from 'solid-js';

const Elevation = props => {
  const merged = mergeProps({ isReversed: false }, props);

  const isSizeDesk = props.deskSize ? `is-${props.deskSize}-desktop` : '';
  const isOffSetDesk = props.deskSize ? `is-offset-${(12 - props.deskSize) / 2}-desktop` : '';
  const isSizeTab = props.tabSize ? `is-${props.tabSize}-tablet` : '';
  const isOffSetTab = props.tabSize ? `is-offset-${(12 - props.tabSize) / 2}-tablet` : '';

  return (
    <div class={`column ${isSizeDesk} ${isOffSetDesk} ${isSizeTab} ${isOffSetTab} with-elevation`}>
      <div class={`columns ${merged.isReversed ? 'reverse-columns' : ''}`}>{props.children}</div>
    </div>
  );
};

export default Elevation;
