import StateProvider from './StateProvider';
import StateConsumer from './containers/StateConsumer';

const App = () => (
  <StateProvider>
    <StateConsumer />
  </StateProvider>
);

export default App;
